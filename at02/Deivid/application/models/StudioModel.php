<?php

require __DIR__ . '/vendor/autoload.php';

//if (php_sapi_name() != 'cli') {
//    throw new Exception('This application must be run on the command line.');
//}

class StudioModel{
    private $gdClient;
    private $gdService;

    function __construct(){
        $this->gdClient = $this->getClient();
        $this->gdService = new Google_Service_Calendar($this->gdClient);
    }
/**
 * Retorna um cliente da API autorizado.
 * @return Google_Client o objeto do cliente autorizado
 */
public function getClient()
{   putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\xampp\htdocs\lp2\at02\Deivid\application\models\credentials.json');
    $client = new Google_Client();
    $client->setApplicationName('Google Calendar API PHP Quickstart');
    $client->setScopes(Google_Service_Calendar::CALENDAR_READONLY);
    $client->setAuthConfig('C:\xampp\htdocs\lp2\at02\Deivid\application\credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Carregar o token previamente autorizado de um arquivo, se existir.
    // O arquivo token.json armazena os tokens de acesso e atualização do usuário e é
    // criado automaticamente quando o fluxo de autorização é concluído para a primeiro
    // vez.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // Se não houver nenhum token anterior ou se ele expirou.
    if ($client->isAccessTokenExpired()) {
        // Atualize o token, se possível, caso contrário, busque um novo.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Solicitar autorização do usuário.
            $authUrl = $client->createAuthUrl();
            
            echo "<a href='$authUrl'>Clique no para gerar o código de autorização</a></br>";
            
            echo'Entre com o código de autorização: ';
            echo'<form action="" method="post" target=”_self”>
                    <label for="POST-name">Código:</label>
                    <input id="POST-name" type="text" name="codigo">
                    <input type="submit" value="Autorizar">
                </form>';
            $authCode = (string) $_POST['codigo'];
            //$authCode = trim(fgets(STDIN));

            // Código de autorização do Exchange para um token de acesso.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Verifica se houve um erro.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Salve o token em um arquivo.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}
}

