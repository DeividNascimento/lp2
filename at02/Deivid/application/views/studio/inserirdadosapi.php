<div class="jumbotron" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
    <div class="text-white text-center py-5 px-4 my-5">
        <div>
            <h4 class="card-title h1-responsive pt-3 mb-5 font-bold"><strong>Siga as instruções para autorizar a sua
                    conta
                    GoogleAgenda a interagir com a API</strong></h4>
            <p class="mx-5 mb-5">Quando você clicar no link de resultados da api pela primeira vez, haverá um erro
                solicitando que seja inserido
                um código de autorização do google agenda. Abra o link em outra aba e faça o login utilizando os dados
                abaixo:</p>

            <p class="mx-5 mb-5">email : studiobudapeste@gmail.com senha: Studiobudapeste123</p>

            <p class="mx-5 mb-5">A API Calendar está sendo utilizada como um tipo de sistema de reservas do studio,
                na GoogleAgenda foi criado 3 eventos para serem exemplificados, para adicionar, alterar ou deletar os
                eventos
                é necessário que faça isso no proprio Google Agenda logado na conta do Studio e os dados da API serão
                atualizados conforme modificação feita.</p>



            <p class="mx-5 mb-5">Segue o link do Google Agenda, para executar um exemplo cadastre no GoogleAgenda uma
                reserva(evento)
                do StudioBudapeste.<a
                    href="https://www.google.com/intl/pt-BR/calendar/about/">https://www.google.com/intl/pt-BR/calendar/about/</a>
            </p>

            <p class="mx-5 mb-5">Por exemplo: Nome do evento: Workshop </p>
            <p class="mx-5 mb-5">data/duração: 25/07/2019</p>
        </div>
    </div>
</div>