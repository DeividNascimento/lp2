<section class="mb-4">


    <h2 class="h1-responsive font-weight-bold text-center my-4">Entre em contato</h2>

    <p class="text-center w-responsive mx-auto mb-5">Dúvidas e Sugestões.
    </p>

    <div class="row  col-md-11">


        <div class="col-md-9 mb-md-0 mb-1">
            <form id="contact-form" name="contact-form" action="mail.php" method="POST">


                <div class="row">


                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">Seu nome</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Seu email</label>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="">Assunto</label>
                        </div>
                    </div>
                </div>

                <div class="row">


                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2"
                                class="form-control md-textarea"></textarea>
                            <label for="message">Escreva aqui sua mensagem</label>
                        </div>

                    </div>
                </div>


            </form>

            <div class="text-center text-md-left">
                <a class="btn stylish-color-dark #3E4551" px-3" onclick="document.getElementById('contact-form').submit();">Enviar</a>
            </div>
            <div class="status"></div>
        </div>

        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>Guarulhos, SP, Brasil</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>+ 55 11 4555-8844</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>contato@studiobudapeste.com</p>
                </li>
            </ul>
        </div>


    </div>

</section>

<br />
<div id="map-container-google-2" class="z-depth-1-half map-container" style="height: 200px">
    <iframe
        src="https://maps.google.com/maps?q=Av. Salgado Filho, 3501 - Centro, Guarulhos - SP, 07115-000, Brasil=&output=embed"
        frameborder="0" style="border:0" allowfullscreen></iframe>
</div>