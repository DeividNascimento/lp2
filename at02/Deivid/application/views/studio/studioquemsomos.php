<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="<?=base_url('assets/img/img01.png')?>">
            <div class="carousel-caption">
                <div class="animated fadeInUp">
                    <h3 class="h3-responsive brown-lighter-hover">
                        Somos um Studio focado em locação e adaptação do espaço para diversos usos.
                        Um local preparado para se adpatar a qualquer necessidade.
                        Nos diga qual é a sua e proporcionaremos uma experiencia única de conforto e profissionalismo.
                    </h3>

                </div>
            </div>
        </div>
    </div>



    <section class="team-section text-center my-5">


        <h2 class="h1-responsive font-weight-bold my-5">Depoimentos e Comentários</h2>

        <p class="dark-grey-text w-responsive mx-auto mb-5">Algumas opiniões e comentários de quem já teve a experiencia
            de estar em nosso Studio.
        </p>


        <div class="row text-center">


            <div class="col-md-4 mb-md-0 mb-5">

                <div class="testimonial">

                    <div class="avatar mx-auto">
                        <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(2).jpg"
                            class="rounded-circle z-depth-1 img-fluid">
                    </div>

                    <h4 class="font-weight-bold dark-grey-text mt-4">Ana Oliveira</h4>
                    <h6 class="font-weight-bold blue-text my-3">Publicitária</h6>
                    <p class="font-weight-normal dark-grey-text">
                        <i class="fas fa-quote-left pr-2"></i>Super recomendo, uma experiência maravilhosa</p>

                    <div class="orange-text">
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star-half-alt"> </i>
                    </div>
                </div>

            </div>



            <div class="col-md-4 mb-md-0 mb-5">

                <div class="testimonial">

                    <div class="avatar mx-auto">
                        <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(9).jpg"
                            class="rounded-circle z-depth-1 img-fluid">
                    </div>

                    <h4 class="font-weight-bold dark-grey-text mt-4">João Santos</h4>
                    <h6 class="font-weight-bold blue-text my-3">Fotografo</h6>
                    <p class="font-weight-normal dark-grey-text">
                        <i class="fas fa-quote-left pr-2"></i>Eu precisava de um local para ensaio fotografico, foi maravilhoso,
                        luz do local incrivel</p>

                    <div class="orange-text">
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                    </div>
                </div>

            </div>



            <div class="col-md-4">

                <div class="testimonial">

                    <div class="avatar mx-auto">
                        <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(12).jpg"
                            class="rounded-circle z-depth-1 img-fluid">
                    </div>

                    <h4 class="font-weight-bold dark-grey-text mt-4">Sara Meleck</h4>
                    <h6 class="font-weight-bold blue-text my-3">Paisagista</h6>
                    <p class="font-weight-normal dark-grey-text">
                        <i class="fas fa-quote-left pr-2"></i>Espaço calmo, me ajudou muito na concentração e foco.</p>

                    <div class="orange-text">
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="fas fa-star"> </i>
                        <i class="far fa-star"> </i>
                    </div>
                </div>

            </div>

        </div>


    </section>