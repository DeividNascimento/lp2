<div class="row">

    <div class="col-md-6 mb-4">

        <div class="card gradient-card">
            <div>
                <a href="#!">
                    <div class="text-white d-flex h-100 mask blue-gradient-rgba">
                        <div class="first-content align-self-center p-3">
                            <h3 class="card-title">Quais são as principais características da API e suas finalidades?
                            </h3>
                        </div>
                        <div class="second-content align-self-center mx-auto text-center">
                        </div>
                    </div>
                </a>

            </div>
            <div class="card-body white">
                <p class="text-muted" align="justify">API Calendar tem como sua principal caracteristica o resgitro
                    de eventos de quaiquer tipo, funcionando
                    como uma agenda. A API tem como finalidade captar os dados de eventos registrados no google agenda
                    por qualquer usuario.
                </p>
            </div>
        </div>
    </div>



    <div class="col-md-6 mb-4">
        <div class="card gradient-card">
            <div>
                <a href="#!">
                    <div class="text-white d-flex h-100 mask purple-gradient-rgba">
                        <div class="first-content align-self-center p-3">
                            <h3 class="card-title">O quê é possível criar com o uso desta API?
                            </h3>
                        </div>
                        <div class="second-content  align-self-center mx-auto text-center">

                        </div>
                    </div>
                </a>
            </div>
            <div class="card-body white">
                <p class="text-muted" align="justify">É possivel criar aplicações cuja base dedados seja datas de compromissos,
                reuniões, eventos, viagens , reservas, e sincroniza-las para que varias pessoas por exemplo, possam se planejar com uma ferramenta
                de uso coletivo.
                </p>
            </div>
        </div>
    </div>    
    <div class="col-md-6 mb-4">        
        <div class="card gradient-card">
            <div>              
                <a href="#!">
                    <div class="text-white d-flex h-100 mask peach-gradient-rgba">
                        <div class="first-content align-self-center p-3">
                            <h3 class="card-title">Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? 
                            </h3>
                        </div>
                        <div class="second-content  align-self-center mx-auto text-center">

                        </div>
                    </div>
                </a>

            </div>           
            <div class="card-body white animated">
                <p class="text-muted" align="justify">A API só pode ser usada por um usuario Google Agenda, ela não tem custo,
                e a documentação é razoavel mas exige outras pesquisas para complementa-la.
                </p>
            </div>
        </div>   
    </div>    
    <div class="col-md-6 mb-4">
       
        <div class="card gradient-card">

            <div>                
                <a href="#!">
                    <div class="text-white d-flex h-100 mask aqua-gradient-rgba">
                        <div class="first-content align-self-center p-3">
                            <h3 class="card-title">Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?
                            </h3>
                        </div>
                        <div class="second-content  align-self-center mx-auto text-center">

                        </div>
                    </div>
                </a>

            </div>           
            <div class="card-body white">
                <p class="text-muted" align="justify">Incialmente houve a necessidade de se criar um projeto API 
                no proprio site do google calendar, e gerar as credenciais necessárias para sua utilização. Após isso 
                se viu necessário também instalar o composer para manter os gerenciamento das libraries.
                Foi preciso indicar um tutorial para o usuario poder liberar o acesso da API para sua conta do Google Agenda
                através de uma chave.Já que é de lá que a os dados são retirados, sendo necessário fazer isso apenas para o primeiro acesso.
                </p>
            </div>

        </div>
       

    </div>
   

</div>
