<?php 

// Obtenha o cliente da API e construa o objeto de serviço.
 
$service = new Google_Service_Calendar($client);

// Imprima os próximos 10 eventos no calendário do usuário.
$calendarId = 'primary';
$optParams = array(
  'maxResults' => 10,
  'orderBy' => 'startTime',
  'singleEvents' => true,
  'timeMin' => date('c'),
);
$results = $service->events->listEvents($calendarId, $optParams);
$events = $results->getItems();

if (empty($events)) {
    print "No upcoming events found.\n";
} else 
        print "Upcoming events:\n";
        ?><table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Nome do Evento</th>
            <th scope="col">Data/Duração do evento</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($events as $event):
            $start = $event->start->dateTime;
            if (empty($start)) {
                $start = $event->start->date;
            }?>

        <tr>
            <td><?= $event->getSummary()?></td>
            <td><?= $start?></td>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </tbody>
</table>