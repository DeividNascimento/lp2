<div class="container mt-2 mb-2 text-center">
    <div class="col-md-12">
        <div class="avatar mx-auto my-auto ">
            <img height='250px' width='250px' src="<?= base_url('assets/img/img04.jpg')?>"
                class="rounded-circle z-depth-1 img-fluid">
        </div>

        <h4 class="font-weight-bold dark-grey-text mt-4">Deivid Magno Marques Nascimento</h4>
        <h6 class="font-weight-bold blue-text my-3">GU3002942</h6>
        <h6 class="font-weight-bold blue-text my-3">Site para exemplificar a  API Calendar</h6>
        <p class="font-weight-normal dark-grey-text">
            <i class="fas fa-globe pr-2"></i>IFSP - Instituto Federal de São Paulo</p>
    </div>
</div>

<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">

    <ol class="carousel-indicators">
        <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-2" data-slide-to="1"></li>
        <li data-target="#carousel-example-2" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <div class="view">
                <img class="d-block w-100" src="<?= base_url('assets/img/img01.png')?>" alt="First slide">
                <div class=""></div>
            </div>
            <div class="carousel-caption">
                <h3 class="h3-responsive">Profissionalismo</h3>

            </div>
        </div>
        <div class="carousel-item">

            <div class="view">
                <img class="d-block w-100" src="<?= base_url('assets/img/img02.png')?>" alt="Second slide">
                <div class=""></div>
            </div>
            <div class="carousel-caption">
                <h3 class="h3-responsive">Ambiente Espaçoso</h3>

            </div>
        </div>
        <div class="carousel-item">

            <div class="view">
                <img class="d-block w-100" src="<?= base_url('assets/img/img03.png')?>" alt="Third slide">
                <div class=""></div>
            </div>
            <div class="carousel-caption">
                <h3 class="h3-responsive">Flexivel</h3>

            </div>
        </div>
    </div>


    <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>



</div>