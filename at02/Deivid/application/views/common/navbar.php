<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark stylish-color-dark
#3E4551">
    <a class="navbar-brand" href="<?= base_url('Studio')?>">StudioBudapeste</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
        aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
        <ul class="navbar-nav mr-auto">

            
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Studio/quemsomos')?>">Quem somos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Studio/inserirdadosapi')?>">Inserir dados API</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Studio/resultadoapi')?>">Resultados API</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Studio/sobreapi')?>">Sobre API</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Studio/contato')?>">Contato</a>
            </li>

        </ul>
        
    </div>
</nav>
<!--/.Navbar -->