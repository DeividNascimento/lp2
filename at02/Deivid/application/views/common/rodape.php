<footer class="page-footer font-small teal pt-4 stylish-color-dark
#3E4551">


    <div class="container-fluid text-center text-md-left">


        <div class="row">


            <div class="col-md-6 mt-md-0 mt-3">


                <h5 class="text-uppercase font-weight-bold">Aconchegante e Profissional</h5>
                <p>Um Studio pronto para ser seu do seu jeito, com a forma que desejar e pelo tempo que precisar.</p>

            </div>


        </div>


    </div>

</footer>


<footer class="page-footer font-small teal pt-4 stylish-color-dark
#3E4551">


    <div class="container">


        <div class="row">


            <div class="col-md-12 py-5">
                <div class="mb-5 flex-center">


                    <a class="fb-ic">
                        <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>

                    <a class="tw-ic">
                        <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>

                    <a class="gplus-ic">
                        <i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>

                    <a class="li-ic">
                        <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>

                    <a class="ins-ic">
                        <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>

                    <a class="pin-ic">
                        <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
                    </a>
                </div>
            </div>


        </div>


    </div>



    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
    </div>


</footer>