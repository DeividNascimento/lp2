<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Studio extends CI_Controller
{

    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('studio/intro');
        $this->load->view('common/footer');
        $this->load->view('common/rodape');
    }
    
    public function quemsomos()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('studio/studioquemsomos');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function sobreapi()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('studio/sobreapi');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function resultadoapi()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('StudioModel');
        $data['client'] = $this->StudioModel->getClient();;
        $this->load->view('studio/resultadoapi',$data);
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function inserirdadosapi()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('studio/inserirdadosapi');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    
    public function contato()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('studio/contatostudio');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }
       
    
    
}

        
